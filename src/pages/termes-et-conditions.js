import Meta from "../components/seo/index";
import { Layout } from "../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

const TermsAndConditions = () => {
  return (
    <>
      <Meta
        title="Termes et conditions | INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/90bYz00t/thumbnail-contact.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid css={{ height: "50em", paddingTop: "20%", margin: "3em" }}>
          <Text span>Termes et conditions</Text>
        </Grid>
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default TermsAndConditions;
