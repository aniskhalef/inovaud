import { Layout } from "../layouts/layout";
import { Grid, Text, Input, Textarea } from "@nextui-org/react";

import Meta from "../components/seo/index";
import { CONSTANTS } from "../constants/index";
import axios from "axios";

import React, { useState } from "react";
import Animation from "/public/assets/lotties/shapes.json";
import Lottie from "react-lottie";
import toast, { Toaster } from "react-hot-toast";

const Contact = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [tel, setTel] = useState("");
  const [date, setDate] = useState("");
  const [message, setMessage] = useState("");

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleTelChange = (event) => {
    setTel(event.target.value);
  };
  const handleDateChange = (event) => {
    setDate(event.target.value);
  };
  const handleMessageChange = (event) => {
    setMessage(event.target.value);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const url = `${CONSTANTS.API_URL_PROD}/user/contact`;

    axios
      .post(url, { email, name, tel, date, message })
      .then((response) => {
        console.log(response);
        toast.success("Demande de rendez-vous envoyé !");
        setEmail("");
        setName("");
        setTel("");
        setDate("");
        setMessage("");
      })
      .catch((error) => {
        console.log(error);
        toast.error("Erreur lors de l'envoi.");
      });
  };
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={250}
        width={360}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Rendez-vous | INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/90bYz00t/thumbnail-contact.png"
        keywords=""
      ></Meta>
      <Layout>
        <Toaster
          position="top-center"
          toastOptions={{
            duration: 6000,
          }}
        />
        <Grid className="shape1">
          <HandleAnimation />
        </Grid>
        <Grid className="career-container" sm={6}>
          <Grid align="center">
            <Text b style={{ fontSize: "25px" }}>
              RÉSERVER UN RENDEZ-VOUS{" "}
            </Text>
            <br></br>
            <br></br>
            <Text span css={{ fontSize: "18px" }}>
              Vous pouvez contacter l&apos;un de nos experts, nous serons
              toujours heureux de vous aider. <br></br>Demandez un rendez-vous
              en remplissant ce formulaire. Nous vous reviendrons dans les plus
              brefs délais.
            </Text>
            <br></br>
            <br></br>
            <br></br>

            <Grid css={{ textAlign: "left" }}>
              <Grid>
                <label htmlFor="name">Votre nom</label>
                <br></br>
                <Input
                  css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                  placeholder="Votre nom"
                  value={name}
                  onChange={handleNameChange}
                  animated={false}
                  fullWidth
                />
              </Grid>
              <br></br>
              <Grid>
                <label htmlFor="email">Votre adresse e-mail</label>
                <br></br>
                <Input
                  type="email"
                  fullWidth
                  placeholder="Votre adresse e-mail"
                  animated={false}
                  value={email}
                  onChange={handleEmailChange}
                  css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                />
              </Grid>
              <br></br>

              <Grid>
                <label htmlFor="number">Votre numéro de téléphone</label>
                <br></br>
                <Input
                  css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                  fullWidth
                  type="number"
                  labelLeft="+222"
                  placeholder="00 000 0000"
                  animated={false}
                  value={tel}
                  onChange={handleTelChange}
                />
              </Grid>
              <br></br>
              <label htmlFor="disponibilite">Votre disponibilité</label>
              <br></br>
              <Input
                placeholder="Veuillez sélectionner une date"
                css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                animated={false}
                fullWidth
                width="186px"
                type="date"
                value={date}
                onChange={handleDateChange}
              />
              <br></br>
              <br></br>

              <Grid>
                <label htmlFor="message">Message</label>
                <br></br>
                <Textarea
                  css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                  fullWidth
                  placeholder="Écrire quelque chose ici..."
                  row={10}
                  animated={false}
                  value={message}
                  onChange={handleMessageChange}
                  minRows={5}
                />
              </Grid>
            </Grid>
          </Grid>
          <br></br>
          <Grid align="center">
            <button
              style={{ minWidth: "100%" }}
              className="main-button"
              onClick={handleSubmit}
            >
              <Text span css={{ fontSize: "18px" }}>
                Envoyer
              </Text>
            </button>
          </Grid>
        </Grid>
        <Grid className="shape2">
          <HandleAnimation />
        </Grid>
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default Contact;
