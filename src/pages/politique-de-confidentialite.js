import { Layout } from "../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../components/seo/index";

const PrivacyPolicy = () => {
  return (
    <>
      <Meta
        title="Politique de confidentialité | INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/90bYz00t/thumbnail-contact.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid css={{ height: "50em", paddingTop: "20%", margin: "3em" }}>
          <Text span>Politique de confidentialité</Text>
        </Grid>
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default PrivacyPolicy;
