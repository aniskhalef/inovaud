import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../../components/seo/index";

import React from "react";

import Animation from "/public/assets/lotties/webdev.json";
import Lottie from "react-lottie";
import { ProcessWeb } from "../../components/landing/services/processweb";

const WebDevPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={360}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Développement Web | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/services/developpement-web"
        thumbnail="https://i.postimg.cc/5t3VrGJj/thumbnail-dev-web.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          css={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>Développement Web</Text>
            <br></br>
            <Text span>
              Qui peut se passer d'un site web de nos jours ? C'est un canal de
              communication et de distribution qui vous assure visibilité,
              confiance et recognition. Concevoir votre site sur la base de vos
              attentes puis le mettre en service est le travail de nos
              développeurs web qui assureront ensuite sa maintenance et sa
              responsivité (adaptation à différents appareils et tailles
              d'écran). Nos experts s'engagent à répondre à vos besoins soit en
              site web personnalisé soit en développement sur CMS
            </Text>
          </Grid>
          <Grid md={6} sm={12} xs={12} align="center">
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <ProcessWeb />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default WebDevPage;
