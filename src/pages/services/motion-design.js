import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../../components/seo/index";

import React from "react";
import Animation from "/public/assets/lotties/motion.json";
import Lottie from "react-lottie";
import { ProcessMotion } from "./../../components/landing/services/processmotion";
const MotionPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={260}
        width={360}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Motion Desgin | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/services/motion-design"
        thumbnail="https://i.postimg.cc/3xcwS0BV/thumbnail.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          style={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>Motion Design</Text>
            <br></br>

            <Text span>
              Imaginez-vous l'identité visuelle de votre entreprise au travers
              d'animations ? Si tel est le cas, notre équipe de Motion Design se
              mettra avec enthousiasme à votre service pour imaginer et créer
              des vidéos promotionnelles ou explicatives, animer votre logo ou
              concevoir des expériences de réalité virtuelle et augmentée.
              L'ensemble de ces contenus – visuels et sonores – travaillerons à
              la mémoire de votre nom, à votre recognition.
            </Text>
            <br></br>
            <br></br>
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <ProcessMotion />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default MotionPage;
