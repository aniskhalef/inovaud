import React from "react";
import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../../components/seo/index";

import Animation from "/public/assets/lotties/iot.json";
import Lottie from "react-lottie";
import { ProcessIot } from "./../../components/landing/services/processiot";

const IotPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={450}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="IOT | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/services/iot"
        thumbnail="https://i.postimg.cc/X7rsX7j4/thumbnail-IOT.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          style={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>IOT</Text>
            <br></br>

            <Text span>
              L'internet des objets (Internet of Things – IoT) désigne le réseau
              interconnecté de dispositifs physiques – objets, lieux,
              environnements – dotés de capteurs, de logiciels et d'une
              connectivité réseau permettant d'échanger et de collecter toutes
              données utiles à l'analyse puis à l'optimisation d'une activité.
              De nombreux agriculteurs connaissent déjà en temps réel l'état de
              santé de leurs vaches ou le besoin en irrigation de leurs
              cultures. Avezvous aussi besoin d'indicateurs qui vous
              permettraient d'anticiper et de prendre des décisions pertinentes
              ?
            </Text>
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <ProcessIot />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default IotPage;
