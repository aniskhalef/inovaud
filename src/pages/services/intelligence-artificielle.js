import React from "react";
import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";
import { ProcessAi } from "../../components/landing/services/processai";

import Meta from "../../components/seo/index";
import Lottie from "react-lottie";
import Animation from "/public/assets/lotties/ai.json";

const AiPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={380}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Intelligence Artificielle | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/services/intelligence-artificielle"
        thumbnail="https://i.postimg.cc/sxG3gQwb/thumbnail-Intelligence-artificiel.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          style={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>Intelligence Artificielle</Text>
            <Text span>
              L'intelligence artificielle (IA) est la simulation de
              l'intelligence humaine dans des machines programmées pour penser
              et agir comme des humains. Inovaud fournit à la fois une expertise
              technique et un soutien réglementaire pour développer des
              algorithmes de compréhension du langage naturel, de reconnaissance
              d'images, d'apprentissage par l'expérience et de prise de
              décision.
            </Text>
            <br></br>
            <br></br> <br></br>
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <ProcessAi />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default AiPage;
