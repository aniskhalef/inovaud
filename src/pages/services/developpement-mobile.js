import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../../components/seo/index";
import { ProcessMobile } from "../../components/landing/services/processmobile";

import React from "react";

import Animation from "/public/assets/lotties/mobiledev.json";
import Lottie from "react-lottie";
const MobileDevPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={360}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Développement Mobile | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/services/developpement-mobile"
        thumbnail="https://i.postimg.cc/Dz1s80YJ/thumbnail-dev-mobile.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          style={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>Développement Mobile</Text>
            <br></br>
            <Text span>
              Une application mobile est un canal de communication et de
              distribution devenu indispensable. Notre équipe Développement
              Mobile est à votre service pour concevoir et lancer une nouvelle
              application – IOS ou Android – ou pour optimiser toute solution
              existante. La réalité augmentée et la réalité virtuelle peuvent
              faire partie de la solution afin d'offrir une expérience
              utilisateur personnalisée et stimulante.
            </Text>
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <ProcessMobile />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default MobileDevPage;
