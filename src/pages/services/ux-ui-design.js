import React from "react";

import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../../components/seo/index";

import Animation from "/public/assets/lotties/uxui.json";
import Lottie from "react-lottie";
import { ProcessUxUi } from "./../../components/landing/services/processuxui";

const UxUiPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={300}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="UX/UI Design | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/ux-ui-design"
        thumbnail="https://i.postimg.cc/3xcwS0BV/thumbnail.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          style={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>Ux/Ui Design</Text>
            <br></br>

            <Text span>
              Nos UX/UI designers œuvrent à l'amélioration continuelle de
              l'expérience utilisateur, de ce que l'usager de votre site ou de
              votre application perçoit et ressent. En concevant des interfaces
              sensoriellement attrayantes, faciles à utiliser et au contenu
              pertinent, nous vous garantissons des solutions efficaces qui
              répondent à l'ensemble de vos attentes.
              <br></br>
            </Text>
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <ProcessUxUi />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default UxUiPage;
