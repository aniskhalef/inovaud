import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../../components/seo/index";

import React from "react";
import Animation from "/public/assets/lotties/blockchain.json";
import Lottie from "react-lottie";
import { ProcessBlockchain } from "./../../components/landing/services/processblockchain";
const BlockchainPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={360}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Blockchain | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/services/blockchain"
        thumbnail="https://i.postimg.cc/QxVTc0yr/thumbnail-blockchain.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          justify={"center"}
          style={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>Blockchain</Text>
            <br></br>

            <Text span>
              La blockchain est un registre de base de données décentralisé,
              immuable et transparent. Nos experts en blockchain s'engagent à
              vous fournir une DApp (application décentralisée) au service de
              votre vision. Ils vous aideront à identifier les possibilités
              d'utilisation de cette technologie avancée pour optimiser vos
              transactions, réduire vos coûts et créer de nouveaux modèles
              d'affaires.
            </Text>
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <ProcessBlockchain />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default BlockchainPage;
