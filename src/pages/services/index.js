import { Services } from "../../components/landing/services";

import { Layout } from "../../layouts/layout";
import { Grid } from "@nextui-org/react";

import Meta from "../../components/seo/index";

import React from "react";

const ServicesPage = () => {
  return (
    <>
      <Meta
        title="Services | INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/3xcwS0BV/thumbnail.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid css={{ marginTop: "10%" }}>
          <Services />
        </Grid>
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default ServicesPage;
