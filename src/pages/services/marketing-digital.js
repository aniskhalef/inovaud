import { Layout } from "../../layouts/layout";
import { Grid, Text } from "@nextui-org/react";

import Meta from "../../components/seo/index";

import React from "react";
import Animation from "/public/assets/lotties/marketing.json";
import Lottie from "react-lottie";
import { ProcessMarketing } from "./../../components/landing/services/processmarketing";

const DigitalMarketingPage = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={360}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Marketing Digital | INOVAUD"
        description=""
        ogUrl="https://inovaud.com/services/marketing-digital"
        thumbnail="https://i.postimg.cc/Qdmn5WrK/thumbnail-digital-marketing.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid.Container
          align={"center"}
          style={{
            display: "inline-block",
            margin: "auto",
            padding: "5em",
            marginTop: "6em",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              textAlign: "left",
            }}
          >
            <Text h1>Marketing Digital</Text>
            <br></br>
            <Text span>
              Nous entendons par marketing digital l'utilisation de canaux
              digitaux pour promouvoir votre projet, votre vision. Passionnés,
              nos spécialistes travaillerons à votre visibilité, à votre
              empreinte, à votre recognition.
            </Text>
          </Grid>
          <Grid md={6} sm={12} xs={12}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
        <br></br>
        <ProcessMarketing />
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default DigitalMarketingPage;
