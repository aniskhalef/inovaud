import { Hero } from "../components/landing/hero";
import { Services } from "../components/landing/services";
import { Solutions } from "../components/landing/solutions";
import { Workflow } from "../components/landing/workflow";
import { Value } from "../components/landing/value";
import { Contact } from "../components/landing/contact";
import { Box } from "../components/styles/box";

import { Layout } from "../layouts/layout";

import Meta from "../components/seo/index";

import React from "react";

const Home = () => {
  return (
    <>
      <Meta
        title="INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/3xcwS0BV/thumbnail.png"
        keywords=""
      ></Meta>
      <Box as="main">
        <Layout>
          <Hero />
          <Services />
          <Solutions />
          <Workflow />
          <Value />
          <Contact />
        </Layout>
      </Box>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default Home;
