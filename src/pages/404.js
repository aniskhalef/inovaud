import React from "react";
import Link from "next/link";
import { useState, useEffect } from "react";

export default function Error404() {
  const text = [
    "Désolé, la page que vous recherchez est introuvable. Merci de vérifier l'URL ou de contacter l'administrateur du site.",
  ];
  const [output, setOutput] = useState("");
  const [lineIndex, setLineIndex] = useState(0);
  const [charIndex, setCharIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      if (
        text &&
        text[lineIndex] &&
        text[lineIndex][charIndex] &&
        lineIndex < text.length - 1
      ) {
        setOutput((output) => `${output}${text[lineIndex][charIndex]}`);
        setCharIndex((charIndex) => charIndex + 1);
      } else if (
        text &&
        text[lineIndex] &&
        text[lineIndex][charIndex] &&
        lineIndex === text.length - 1
      ) {
        setOutput((output) => `${output}${text[lineIndex][charIndex]}`);
        setCharIndex((charIndex) => charIndex + 1);
      } else {
        setTimeout(() => {
          setLineIndex((lineIndex) => lineIndex + 1);
          setCharIndex(0);
          setOutput((output) => `${output}\n`);
        }, 1000);
      }
    }, 50);

    // cleanup function to clear the interval
    return () => clearInterval(interval);
  }, [lineIndex, charIndex]);

  return (
    <>
      <div className="px-6 py-6">
        <h3 style={{ fontSize: "50px" }}>Error 404</h3>
        <br></br>
        <h3>{output}</h3>
        <br></br>
        <h3>
          Nous vous invitons à <Link href="/">retourner en arrière</Link> ou à{" "}
          <Link href="/">accéder à la page d&apos;accueil du site.</Link>.
        </h3>
      </div>
    </>
  );
}
