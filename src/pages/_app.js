import "../styles/globals.css";
import { createTheme, NextUIProvider } from "@nextui-org/react";
import { ThemeProvider as NextThemesProvider } from "next-themes";
import { SSRProvider } from "@react-aria/ssr";

import { Analytics } from "@vercel/analytics/react";
import React from "react";

import App from "next/app";
import "../styles/tailwind.css";

import "@fortawesome/fontawesome-free/css/all.min.css";

const darkTheme = createTheme({
  type: "dark",
  theme: {
    colors: {},
  },
});

export default class MyApp extends App {
  componentDidMount() {
    let comment = document.createComment(`
`);
    document.insertBefore(comment, document.documentElement);
  }

  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    const Layout = Component.layout || (({ children }) => <>{children}</>);

    return (
      <NextThemesProvider
        defaultTheme="system"
        attribute="class"
        value={{
          dark: darkTheme.className,
        }}
      >
        <NextUIProvider theme={darkTheme}>
          <SSRProvider>
            <Layout>
              <Component {...pageProps} />
            </Layout>
            <Analytics />
          </SSRProvider>
        </NextUIProvider>
      </NextThemesProvider>
    );
  }
}
