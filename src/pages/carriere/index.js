import React from "react";
import { Grid, Input, Text } from "@nextui-org/react";

import { Layout } from "../../layouts/layout";
import { Box } from "../../components/styles/box";
import Animation from "/public/assets/lotties/career.json";

import Meta from "../../components/seo/index";
import Lottie from "react-lottie";
import { Offers } from "../../components/offers";
import Link from "next/link";

const Career = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={350}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Carrière | INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/dVMj12Nj/thumbnail-carri-re.png"
        keywords=""
      ></Meta>
      <Box as="main">
        <Layout>
          <Grid.Container
            align={"center"}
            css={{
              margin: "auto",
              width: "100%",
              padding: "6em",
              alignItems: "center",
              marginTop: "6%",
            }}
          >
            <Grid
              md={6}
              sm={12}
              xs={12}
              css={{
                textAlign: "left",
              }}
            >
              <Grid>
                <Text h1>Carrière</Text>
                <br></br>
                <Text span>
                  L’équipe Inovaud est toujours impatiente d’accueillir des
                  membres talentueux et ouverts d’esprit.
                  <br></br>Postulez pour faire partie de l’équipe où vous serez
                  en mesure d’apprendre en continu et de relever des défis
                </Text>
              </Grid>
            </Grid>
            <Grid md={6} sm={12} xs={12}>
              <HandleAnimation />
            </Grid>
          </Grid.Container>
          <br></br>
          <Offers />

          <Grid
            style={{
              padding: "6em",
            }}
          >
            <Text h1>Candidature Spontanée</Text>
            <br></br>
            <Text span>
              Candidature spontanée ou à un poste ouvert, n'hésitez pas à nous
              faire part de vos motivations en vue d'une future collaboration.
            </Text>
            <br></br>
            <br></br>
            <Grid>
              <Link href={"/carriere/candidature"}>
                <button className="main-button">Postuler maintenant</button>
              </Link>
            </Grid>
          </Grid>
        </Layout>
      </Box>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default Career;
