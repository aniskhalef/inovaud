import React from "react";
import { Grid, Badge, Text } from "@nextui-org/react";
import Link from "next/link";
import Image from "next/image";
import { Layout } from "../../layouts/layout";
import Animation from "/public/assets/lotties/shapes.json";

import Meta from "../../components/seo/index";
import Lottie from "react-lottie";

const Career = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={250}
        width={300}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Carrière Développeur Web | INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/dVMj12Nj/thumbnail-carri-re.png"
        keywords=""
      ></Meta>
      <Layout>
        <Grid className="shape1">
          <HandleAnimation />
        </Grid>
        <Grid>
          <Grid className="career-container" sm={6}>
            <Link href="/carriere">
              <Image
                src="/assets/icons/backarrow.svg"
                width={30}
                height={30}
                alt=""
                style={{ cursor: "pointer" }}
              />
            </Link>
            <br></br>
            <br></br>
            <Grid>
              <Text
                span
                style={{
                  fontWeight: "bold",
                  fontSize: "25px",
                }}
              >
                Developpeurs web
              </Text>

              <hr
                style={{
                  border: "solid #fff 1px",
                  marginBottom: "8px",
                  width: "10%",
                }}
              ></hr>

              <Grid style={{ marginBottom: "16px" }}>
                <Text span>03/Avril 2023</Text>
              </Grid>
              <Grid>
                <Badge
                  isSquared
                  css={{
                    background: "#3C8A2E",
                    border: "none",
                    width: "6em",
                    fontSize: "15px",
                  }}
                >
                  Backend
                </Badge>
              </Grid>
            </Grid>
            <br></br>

            <Grid>
              <Grid>
                <Text b size={"$xl"}>
                  Description
                </Text>
                <br></br>
                <Text className="content" span size={"$md"}>
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero
                </Text>
              </Grid>
              <br></br>
              <Grid>
                <Text b size={"$xl"}>
                  Mission
                </Text>
                <br></br>
                <Text span>
                  <ul className="ul">
                    <Text className="content">
                      <li className="li">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                        sed diam nonumy eirmod tempor invidunt ut labore et
                        dolore magna aliquyam erat, sed diam voluptua.
                      </li>
                    </Text>

                    <Text className="content" span>
                      <li className="li">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                        sed diam nonumy eirmod tempor invidunt ut labore et
                        dolore magna aliquyam erat, sed diam voluptua.
                      </li>
                    </Text>
                    <Text className="content" span>
                      <li className="li">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                        sed diam nonumy eirmod tempor invidunt ut labore et
                        dolore magna aliquyam erat, sed diam voluptua.
                      </li>
                    </Text>
                  </ul>
                </Text>
              </Grid>
              <br></br>
              <Grid>
                <Text b size={"$xl"}>
                  Qualifications
                </Text>
                <br></br>
                <ul className="ul">
                  <Text span className="content">
                    <li className="li">
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                      sed diam nonumy eirmod tempor invidunt ut labore et dolore
                      magna aliquyam erat, sed diam voluptua.
                    </li>
                  </Text>
                  <Text className="content" span>
                    <li className="li">
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                      sed diam nonumy eirmod tempor invidunt ut labore et dolore
                      magna aliquyam erat, sed diam voluptua.
                    </li>
                  </Text>
                  <Text className="content" span>
                    <li className="li">
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                      sed diam nonumy eirmod tempor invidunt ut labore et dolore
                      magna aliquyam erat, sed diam voluptua.
                    </li>
                  </Text>
                </ul>
              </Grid>
            </Grid>
            <br></br>
            <Grid>
              <Link href={"/carriere/candidature"}>
                <button className="main-button">Postuler maintenant</button>
              </Link>
            </Grid>
          </Grid>
        </Grid>
        <Grid className="shape2">
          <HandleAnimation />
        </Grid>
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default Career;
