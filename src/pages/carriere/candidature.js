import { Layout } from "../../layouts/layout";
import { Grid, Text, Input, Textarea } from "@nextui-org/react";
import Image from "next/image";
import Link from "next/link";

import Meta from "../../components/seo/index";
import { CONSTANTS } from "../../constants/index";
import axios from "axios";

import React, { useState } from "react";
import Animation from "/public/assets/lotties/shapes.json";
import Lottie from "react-lottie";
import toast, { Toaster } from "react-hot-toast";

const Candidature = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [cv, setCv] = useState("");
  const [message, setMessage] = useState("");

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleCvChange = (event) => {
    const selectedFile = event.target.files[0];
  };
  const handleMessageChange = (event) => {
    setMessage(event.target.value);
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const url = `${CONSTANTS.API_URL_PROD}/user/apply`;

    const formData = new FormData();
    formData.append("name", name);
    formData.append("email", email);
    formData.append("cv", cv);
    formData.append("message", message);

    axios
      .post(url, formData)
      .then((response) => {
        console.log(response);
        toast.success("Candidature envoyé !");
        setName("");
        setEmail("");
        setCv(null);
        setMessage("");
      })
      .catch((error) => {
        console.log(error);
        toast.error(error.response.data.message);
      });
  };
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={250}
        width={360}
        style={{ pointerEvents: "none" }}
      />
    );
  }
  return (
    <>
      <Meta
        title="Rejoins notre équipe | INOVAUD"
        description=""
        ogUrl="https://inovaud.com"
        thumbnail="https://i.postimg.cc/dVMj12Nj/thumbnail-carri-re.png"
        keywords=""
      ></Meta>
      <Layout>
        <Toaster
          position="top-center"
          toastOptions={{
            duration: 8000,
          }}
        />
        <Grid className="shape1">
          <HandleAnimation />
        </Grid>
        <Grid className="career-container" sm={6}>
          <Link href="/carriere">
            <Image
              src="/assets/icons/backarrow.svg"
              width={30}
              height={30}
              alt=""
              style={{ cursor: "pointer" }}
            />
          </Link>

          <Grid align="center">
            <Text b style={{ fontSize: "25px" }}>
              REJOINS NOTRE ÉQUIPE
            </Text>
            <br></br>
            <br></br>
            <Text span css={{ fontSize: "18px" }}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea
            </Text>
            <br></br>
            <br></br>
            <br></br>
            <Grid css={{ textAlign: "left" }}>
              <Grid>
                <label aria-label="cv-input">Votre nom</label>
                <Input
                  animated={false}
                  placeholder="Votre nom"
                  value={name}
                  onChange={handleNameChange}
                  fullWidth
                  css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                />
              </Grid>
              <br></br>
              <Grid>
                <label aria-label="cv-input">Votre adresse e-mail</label>

                <Input
                  animated={false}
                  fullWidth
                  placeholder="Votre adresse e-mail"
                  value={email}
                  onChange={handleEmailChange}
                  css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                />
              </Grid>
              <br></br>
              <label aria-label="cv-input">Télécharger votre CV</label>

              <Input
                animated={false}
                css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                fullWidth
                placeholder="Télécharger votre CV"
                type="file"
                onChange={handleCvChange}
                id="file"
                name="file"
                style={{ position: "hidden" }}
                contentRightStyling={false}
                contentClickable={true}
                contentRight={
                  <label aria-label="cv-input">
                    <img
                      src="/assets/icons/upload.svg"
                      width={20}
                      height={20}
                      alt=""
                      style={{ cursor: "pointer", marginRight: "30px" }}
                    />
                  </label>
                }
              />
              <br></br>
              <br></br>

              <Grid>
                <label aria-label="message">Message</label>

                <Textarea
                  animated={false}
                  minRows={5}
                  css={{ border: "2px solid #3C8A2E", marginTop: "10px" }}
                  placeholder="Écrire quelque chose ici..."
                  fullWidth
                  value={message}
                  onChange={handleMessageChange}
                />
              </Grid>
            </Grid>
          </Grid>
          <br></br>
          <Grid align="center">
            <button
              className="main-button"
              onClick={handleSubmit}
              style={{ minWidth: "100%" }}
            >
              <Text span css={{ fontSize: "18px" }}>
                Postuler
              </Text>
            </button>
          </Grid>
        </Grid>
        <Grid className="shape2">
          <HandleAnimation />
        </Grid>
      </Layout>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default Candidature;
