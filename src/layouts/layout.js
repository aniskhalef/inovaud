import { Footer } from "./footer";
import { Nav } from "./navbar/navbar";

const Layout = ({ children }) => (
  <>
    <div className="box">
      <Nav />
      {children}
      <Footer />
    </div>
  </>
);

export { Layout };
