import {
  Dropdown,
  Link,
  Navbar,
  Image,
  Text,
  Grid,
  Divider,
} from "@nextui-org/react";
import React from "react";
import { icons } from "./icons";
import { Flex } from "../../components/styles/flex";

export const Nav = () => {
  const collapseItems = [
    {
      href: "/",
      title: "Acceuil",
    },
    {
      href: "/services",
      title: "Services",
    },
    {
      href: "/carriere",
      title: "Carrière",
    },
  ];

  return (
    <Navbar
      maxWidth={"xl"}
      css={{
        "& .nextui-navbar-container": {
          background: "transparent",
          position: "fixed",
          top: 0,
          margin: "auto",
          width: "90%",
        },
      }}
    >
      <Navbar.Brand>
        <Navbar.Toggle aria-label="toggle navigation" showIn="sm" />
        <Link href="/">
          <Image
            width={205}
            height={34}
            src="/assets/svg/logo.svg"
            alt="logo"
            objectFit="contain"
          />
        </Link>
      </Navbar.Brand>

      <Navbar.Content
        hideIn="sm"
        variant="underline"
        activeColor={"success"}
        css={{
          justifyContent: "space-around",
          width: "50%",
        }}
      >
        <Navbar.Link activeClass="active-link" href="/">
          Acceuil
        </Navbar.Link>
        <Navbar.Link href="/services">Services</Navbar.Link>

        <Navbar.Link href="/carriere">Carrière</Navbar.Link>
        <Navbar.Link href="/rendez-vous">
          <button className="main-button ">Contactez-nous</button>
        </Navbar.Link>
      </Navbar.Content>

      <Navbar.Collapse css={{ position: "fixed" }}>
        {collapseItems.map(({ title, href }) => (
          <Navbar.CollapseItem key={title}>
            <Link
              color="inherit"
              css={{
                minWidth: "100%",
              }}
              href={href}
            >
              {title}
            </Link>
          </Navbar.CollapseItem>
        ))}
      </Navbar.Collapse>
    </Navbar>
  );
};
