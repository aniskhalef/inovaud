import React from 'react';
interface Props {
   fill?: string;
   size?: number;
   width?: number;
   height?: number;
}

const ChevronDownIcon = ({
   fill,
   size,
   width = 24,
   height = 24,
   ...props
}: Props) => {
   return (
      <svg
         fill="none"
         height={size || height || 24}
         viewBox="0 0 24 24"
         width={size || width || 24}
         xmlns="http://www.w3.org/2000/svg"
         {...props}
      >
         <path
            d="m19.92 8.95-6.52 6.52c-.77.77-2.03.77-2.8 0L4.08 8.95"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeWidth={1.5}
         />
      </svg>
   );
};

const TagUserIcon = ({
   fill,
   size,
   width = 24,
   height = 24,
   ...props
}: Props) => {
   return (
      <svg
         fill="none"
         height={size || height}
         viewBox="0 0 24 24"
         width={size || width}
         xmlns="http://www.w3.org/2000/svg"
         {...props}
      >
         <path
            d="M18 18.86h-.76c-.8 0-1.56.31-2.12.87l-1.71 1.69c-.78.77-2.05.77-2.83 0l-1.71-1.69c-.56-.56-1.33-.87-2.12-.87H6c-1.66 0-3-1.33-3-2.97V4.98c0-1.64 1.34-2.97 3-2.97h12c1.66 0 3 1.33 3 2.97v10.91c0 1.63-1.34 2.97-3 2.97Z"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeWidth={1.5}
         />
         <path
            d="M12 10a2.33 2.33 0 1 0 0-4.66A2.33 2.33 0 0 0 12 10ZM16 15.66c0-1.8-1.79-3.26-4-3.26s-4 1.46-4 3.26"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}
         />
      </svg>
   );
};

const ServerIcon = ({fill, size, width = 24, height = 24, ...props}: Props) => {
   return (
      <svg
         fill="none"
         height={size || height}
         viewBox="0 0 24 24"
         width={size || width}
         xmlns="http://www.w3.org/2000/svg"
         {...props}
      >
         <path
            d="M19.32 10H4.69c-1.48 0-2.68-1.21-2.68-2.68V4.69c0-1.48 1.21-2.68 2.68-2.68h14.63C20.8 2.01 22 3.22 22 4.69v2.63C22 8.79 20.79 10 19.32 10ZM19.32 22H4.69c-1.48 0-2.68-1.21-2.68-2.68v-2.63c0-1.48 1.21-2.68 2.68-2.68h14.63c1.48 0 2.68 1.21 2.68 2.68v2.63c0 1.47-1.21 2.68-2.68 2.68ZM6 5v2M10 5v2M6 17v2M10 17v2M14 6h4M14 18h4"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}
         />
      </svg>
   );
};

const FlashIcon = ({fill, size, width = 24, height = 24, ...props}: Props) => {
   return (
      <svg
         fill="none"
         height={size || height}
         viewBox="0 0 24 24"
         width={size || width}
         xmlns="http://www.w3.org/2000/svg"
         {...props}
      >
         <path
            d="M6.09 13.28h3.09v7.2c0 1.68.91 2.02 2.02.76l7.57-8.6c.93-1.05.54-1.92-.87-1.92h-3.09v-7.2c0-1.68-.91-2.02-2.02-.76l-7.57 8.6c-.92 1.06-.53 1.92.87 1.92Z"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeWidth={1.5}
         />
      </svg>
   );
};

const ActivityIcon = ({
   fill,
   size,
   width = 24,
   height = 24,
   ...props
}: Props) => {
   return (
      <svg
         data-name="Iconly/Curved/Activity"
         height={size || height || 24}
         viewBox="0 0 24 24"
         width={size || width || 24}
         xmlns="http://www.w3.org/2000/svg"
         {...props}
      >
         <g
            fill="none"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit={10}
            strokeWidth={1.5}
         >
            <path d="M6.918 14.854l2.993-3.889 3.414 2.68 2.929-3.78" />
            <path d="M19.668 2.35a1.922 1.922 0 11-1.922 1.922 1.921 1.921 0 011.922-1.922z" />
            <path d="M20.756 9.269a20.809 20.809 0 01.194 3.034c0 6.938-2.312 9.25-9.25 9.25s-9.25-2.312-9.25-9.25 2.313-9.25 9.25-9.25a20.931 20.931 0 012.983.187" />
         </g>
      </svg>
   );
};

const ScaleIcon = ({fill, size, width = 24, height = 24, ...props}: Props) => {
   return (
      <svg
         fill="none"
         height={size || height}
         viewBox="0 0 24 24"
         width={size || width}
         xmlns="http://www.w3.org/2000/svg"
         {...props}
      >
         <path
            d="M9 22h6c5 0 7-2 7-7V9c0-5-2-7-7-7H9C4 2 2 4 2 9v6c0 5 2 7 7 7ZM18 6 6 18"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}
         />
         <path
            d="M18 10V6h-4M6 14v4h4"
            stroke={fill}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}
         />
      </svg>
   );
};

const BotIcon = ({fill, size, width = 24, height = 24, ...props}: Props) => {
   return (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.28 21.56">
         <defs>
            <style>{`.cls-1{fill:${fill};}`}</style>
         </defs>
         <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
               <path className="cls-1" d="M17.11,8.29a2.75,2.75,0,0,0,.1-.7,2.42,2.42,0,0,0-1.15-2.06L17,3.12l.2,0a1.57,1.57,0,1,0-1.56-1.57,1.52,1.52,0,0,0,.53,1.17L15.21,5.2a3,3,0,0,0-.43,0,2.44,2.44,0,0,0-2.22,1.43,14.54,14.54,0,0,0-2.42-.2,14.73,14.73,0,0,0-2.42.2A2.42,2.42,0,0,0,5.51,5.16a3,3,0,0,0-.43,0l-1-2.46A1.57,1.57,0,0,0,3.09,0a1.57,1.57,0,1,0,0,3.14l.2,0,.93,2.41A2.44,2.44,0,0,0,3.08,7.59a2.37,2.37,0,0,0,.1.7A6.13,6.13,0,0,0,0,13.3c0,3.81,4.55,6.91,10.14,6.91a14.38,14.38,0,0,0,3.63-.45l1.67,1.66a.45.45,0,0,0,.32.14.39.39,0,0,0,.18,0,.46.46,0,0,0,.28-.42V18.84c2.55-1.3,4.06-3.35,4.06-5.54A6.14,6.14,0,0,0,17.11,8.29ZM17.19.91a.66.66,0,0,1,.66.66.66.66,0,1,1-.66-.66ZM2.43,1.57a.66.66,0,0,1,1.32,0,.66.66,0,0,1-1.32,0Zm12.35,4.5A1.53,1.53,0,0,1,16.3,7.59a1.5,1.5,0,0,1,0,.22,12.39,12.39,0,0,0-2.78-1A1.52,1.52,0,0,1,14.78,6.07ZM4,7.59a1.52,1.52,0,0,1,2.8-.81A12.21,12.21,0,0,0,4,7.81,1.5,1.5,0,0,1,4,7.59ZM15.57,18.14a.46.46,0,0,0-.26.41V20l-1.08-1.08a.46.46,0,0,0-.32-.13l-.12,0a13.69,13.69,0,0,1-3.65.49c-5.09,0-9.23-2.69-9.23-6s4.14-6,9.23-6,9.23,2.69,9.23,6C19.37,15.2,18,17,15.57,18.14Z"/>
               <path className="cls-1" d="M6.64,13.18a.82.82,0,0,1-.82.81A.81.81,0,0,1,5,13.18a.82.82,0,0,1,.81-.82A.82.82,0,0,1,6.64,13.18Z"/>
               <path className="cls-1" d="M9.52,13.18A.82.82,0,0,1,8.7,14a.81.81,0,0,1-.81-.81.82.82,0,0,1,.81-.82A.82.82,0,0,1,9.52,13.18Z"/>
               <path className="cls-1" d="M12.4,13.18a.82.82,0,0,1-.82.81.81.81,0,0,1-.81-.81.82.82,0,0,1,.81-.82A.82.82,0,0,1,12.4,13.18Z"/>
               <path className="cls-1" d="M15.28,13.18a.82.82,0,0,1-1.64,0,.82.82,0,0,1,1.64,0Z"/>
            </g>
         </g>
      </svg>
   );
};

export const icons = {
   chevron: <ChevronDownIcon fill="currentColor" size={16} />,
   scale: <ScaleIcon fill="var(--nextui-colors-warning)" size={30} />,
   activity: <ActivityIcon fill="var(--nextui-colors-secondary)" size={30} />,
   flash: <FlashIcon fill="var(--nextui-colors-primary)" size={30} />,
   server: <ServerIcon fill="var(--nextui-colors-success)" size={30} />,
   user: <TagUserIcon fill="var(--nextui-colors-error)" size={30} />,
      bot: <BotIcon fill="var(--nextui-colors-error)" size={30} />,

};
