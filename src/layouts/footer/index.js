import React, { useState } from "react";
import { Input, useInput, Text } from "@nextui-org/react";
import Link from "next/link";
import Image from "next/image";
import axios from "axios";
import { CONSTANTS } from "../../constants/index";
import toast, { Toaster } from "react-hot-toast";

export const Footer = () => {
  const { value, reset, bindings } = useInput("");

  const [email, setEmail] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    const url = `${CONSTANTS.API_URL_PROD}/user/newsletter`;

    try {
      const response = await axios.post(url, { email });
      console.log(response);
      toast.success("Newsletter subscription successful !");
      setEmail("");
    } catch (error) {
      if (error.response.status === 403) {
        toast.error(error.response.data.message);
      } else {
        console.error(error);
        toast.error(error.message);
      }
    }
  };
  return (
    <footer
      className=" bg-black pb-6"
      style={{ padding: "3em", marginTop: "5%" }}
    >
      <Toaster
        position="top-center"
        toastOptions={{
          duration: 8000,
        }}
      />
      <div className="px-4 mx-auto ">
        <div className="w-full lg:w-10/12">
          <div className=" mx-auto flex flex-wrap  md:justify-between">
            <div>
              <div>
                <Link href="/">
                  <Image
                    width={200}
                    height={100}
                    src="/assets/svg/logo.svg"
                    alt="logo"
                    objectFit="contain"
                  />
                </Link>
              </div>
              <div
                className=" flex  "
                style={{ minWidth: "70%", alignItems: "center" }}
              >
                <div>
                  <Image
                    width={30}
                    height={30}
                    src="/assets/icons/swissflag.png"
                    alt="swissflag"
                    objectFit="contain"
                  />
                </div>
                &nbsp; &nbsp;
                <Text span size={"$sm"}>
                  {" "}
                  Inovaud, Rue du Port-Franc 22, 1003 Lausanne
                </Text>
              </div>
              <div className=" flex items-center" style={{ minWidth: "70%" }}>
                <Image
                  width={30}
                  height={30}
                  src="/assets/icons/phone.svg"
                  alt="phone"
                  objectFit="contain"
                />
                &nbsp; &nbsp;
                <Text span size={"$sm"}>
                  + 41 79 192 42 59
                </Text>
              </div>
              <div className=" flex items-center" style={{ minWidth: "70%" }}>
                <Image
                  width={30}
                  height={30}
                  src="/assets/icons/mail.svg"
                  alt="mail"
                  objectFit="contain"
                />
                &nbsp; &nbsp;
                <Text span size={"$sm"}>
                  info@inovaud.ch
                </Text>
              </div>
            </div>

            <div
              className="flex flex-wrap  mb-6 justify-between"
              style={{ width: "60%" }}
            >
              <div className="w-full  py-6 lg:w-3/12 ">
                <Text span className="block uppercase mb-2  footer-title">
                  QUI SOMMES-NOUS?
                </Text>
                <ul className="list-unstyled">
                  <li>
                    <Link href="/">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className=" block pb-2 ">
                          Accueil
                        </Text>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className=" block pb-2 ">
                          Services
                        </Text>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/">
                      <a>
                        <Text span size={"$sm"} className=" block pb-2 ">
                          Carrière
                        </Text>
                      </a>
                    </Link>
                  </li>{" "}
                  <li>
                    <Link href="/">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className=" block pb-2 ">
                          Contact
                        </Text>
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>

              <div className="w-full lg:w-3/12  py-6">
                <Text span className="block uppercase  mb-2 footer-title">
                  NOS SERVICES
                </Text>
                <ul className="list-unstyled">
                  <li>
                    <Link href="/services/developpement-web">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className="block pb-2 ">
                          Développement Web
                        </Text>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/services/developpement-mobile">
                      <a>
                        <Text span size={"$sm"} className=" block pb-2 ">
                          Développement Mobile
                        </Text>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/services/intelligence-artificielle">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className=" block pb-2 ">
                          Intelligence Artificielle
                        </Text>
                      </a>
                    </Link>
                  </li>{" "}
                  <li>
                    <Link href="/services/iot">
                      <a>
                        <Text span size={"$sm"} className=" block pb-2 ">
                          IOT
                        </Text>
                      </a>
                    </Link>
                  </li>{" "}
                  <li>
                    <Link href="/services/blockchain">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className=" block pb-2 ">
                          Blockchain
                        </Text>
                      </a>
                    </Link>
                  </li>{" "}
                  <li>
                    <Link href="/services/ux-ui-design">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className=" block pb-2 ">
                          UX/UI Design
                        </Text>
                      </a>
                    </Link>
                  </li>{" "}
                  <li>
                    <Link href="/services/motion-design">
                      <a>
                        {" "}
                        <Text span size={"$sm"} className="block pb-2">
                          Motion Design
                        </Text>
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>

              <div className="w-full lg:w-4/12 py-6">
                <Text span className="block uppercase mb-2 footer-title">
                  Newsletter
                </Text>

                <div>
                  <Text span size={"$md"}>
                    Inscrivez-vous pour recevoir nos dernières nouvelles et
                    offres.
                  </Text>
                  <div>
                    <br></br>
                    <Input
                      required
                      width="100%"
                      {...bindings}
                      clearable
                      type="email"
                      placeholder="Votre adresse email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      contentRightStyling={true}
                      contentClickable
                      contentRight={
                        <div>
                          <Image
                            src="/assets/icons/sendarrow.svg"
                            width={1000}
                            height={1000}
                            alt=""
                            onClick={handleSubmit}
                          />
                        </div>
                      }
                    />
                  </div>
                  <br></br>
                  <div className="flex justify-self-start">
                    <div style={{ width: "2em" }}>
                      <Image
                        src="/assets/icons/facebook.svg"
                        width={20}
                        height={20}
                        alt="facebook"
                      />
                    </div>
                    <div style={{ width: "2em" }}>
                      <Image
                        src="/assets/icons/instagram.svg"
                        width={20}
                        height={20}
                        alt="instgram"
                      />
                    </div>
                    <div style={{ width: "2em" }}>
                      <Image
                        src="/assets/icons/linkedin.svg"
                        width={20}
                        height={20}
                        alt="linkedin"
                      />
                    </div>
                  </div>
                  <br></br>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr className="my-6 border-blueGray-300" />

        <div className=" flex " style={{ justifyContent: "space-around" }}>
          <div style={{ width: "100%" }}>
            <Text span size={"$sm"}>
              © {new Date().getFullYear()} Copyright tous droits réservés.
            </Text>
          </div>
          <div
            className=" text-center flex"
            style={{ justifyContent: "flex-end" }}
          >
            <div>
              <Link href="/politique-de-confidentialite">
                <a>
                  <Text span size={"$sm"} className="footer-hover">
                    Politique de confidentialité
                  </Text>
                </a>
              </Link>
            </div>
            &nbsp; &nbsp; &nbsp;
            <div>
              <Link href="/termes-et-conditions">
                <a>
                  <Text span size={"$sm"} className="footer-hover">
                    Termes et conditions
                  </Text>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};
