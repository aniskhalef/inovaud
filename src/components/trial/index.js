import { Button, Grid, Text } from "@nextui-org/react";
import React, { useState, useEffect } from "react";
import { Flex } from "../../styles/flex";
import axios from "axios";
import { Input, useInput } from "@nextui-org/react";
import { SendButton } from "./SendButton";
import { SendIcon } from "./SendIcon";
export const Trial = () => {
  const [flashMessage, setFlashMessage] = useState("");
  const [showMessage, setShowMessage] = useState(false);
  const { value, reset, bindings } = useInput("");

  const validateEmail = (value) => {
    return value.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i);
  };

  const helper = React.useMemo(() => {
    if (!value)
      return {
        text: "",
        color: "",
      };
    const isValid = validateEmail(value);
    return {
      text: isValid ? "Correct email" : "Enter a valid email",
      color: isValid ? "success" : "error",
    };
  }, [value]);
  const [email, setEmail] = useState("");

  const handleSubmit = async (e) => {
    setFlashMessage("A confirmation email has been sent to you !");
    setShowMessage(true);
    e.preventDefault();
    try {
      const response = await axios.post("https://back.inovaud/api/subscribe", {
        email,
      });
      console.log(response);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    if (showMessage) {
      setTimeout(() => {
        setShowMessage(false);
      }, 8000);
    }
  }, [showMessage]);
  return (
    <>
      <Flex
        css={{
          py: "$20",
          px: "$6",
        }}
        justify={"center"}
        direction={"column"}
        align={"center"}
      >
        <Text
          h3
          css={{
            display: "inline",
            textGradient: "45deg, #667eea -20%, #6B8DD6 50%",
          }}
        >
          Want to stay up to date with the latest SEO trends and techniques?
          <br></br>Subscribe to our newsletter and get insider access.
        </Text>

        <form onSubmit={handleSubmit}>
          <Flex
            css={{
              gap: "$8",
              pt: "$4",
            }}
            wrap={"wrap"}
          >
            <Grid>
              <Input
                size="lg"
                required
                width="100%"
                {...bindings}
                clearable
                shadow={false}
                onClearClick={reset}
                status={helper.color}
                color={helper.color}
                helperColor={helper.color}
                helperText={helper.text}
                type="email"
                placeholder="Enter your email address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                contentRightStyling={false}
                contentRight={
                  <SendButton>
                    <SendIcon
                      filled={undefined}
                      size={undefined}
                      height={undefined}
                      width={undefined}
                      label={undefined}
                      className={undefined}
                      type="submit"
                    />
                  </SendButton>
                }
              />
            </Grid>
          </Flex>
        </form>
        {showMessage ? (
          <Grid
            css={{
              fontWeight: "bold",
              textGradient: "45deg, #667eea -20%, #6B8DD6 50%",
              padding: "1em",
            }}
          >
            {flashMessage}
          </Grid>
        ) : null}
      </Flex>
    </>
  );
};
