import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import Link from "next/link";

export const Services = () => {
  const servicesArray = [
    {
      icon: "/assets/landing/devweb.svg",
      label: "Développement Web",
      link: "/services/developpement-web",
    },
    {
      icon: "/assets/landing/devmobile.svg",
      label: "Développement Mobile",
      link: "/services/developpement-mobile",
    },
    {
      icon: "/assets/landing/digitalmarketing.svg",
      label: "Digital Marketing",
      link: "/services/marketing-digital",
    },
    {
      icon: "/assets/landing/ai.svg",
      label: "Intelligence Artificielle",
      link: "/services/intelligence-artificielle",
    },
    {
      icon: "/assets/landing/iot.svg",
      label: "IOT",
      link: "/services/iot",
    },
    {
      icon: "/assets/landing/blockchain.svg",
      label: "Blockchain",
      link: "/services/blockchain",
    },
    {
      icon: "/assets/landing/uxui.svg",
      label: "UX/UI Design",
      link: "/services/ux-ui-design",
    },
    {
      icon: "/assets/landing/motion.svg",
      label: "Motion Design",
      link: "/services/motion-design",
    },
  ];

  return (
    <>
      <Grid
        align="center"
        css={{ marginTop: "10em", padding: "1em", textAlign: "center" }}
      >
        <Grid css={{ textAlign: "center" }}>
          <Text h1>NOS SERVICES</Text>
          <br></br>
          <Text h4 css={{ fontWeight: 100, letterSpacing: "1px" }}>
            Nous mettons à votre disposition les fruits de notre expertise et
            savoir-faire. <br></br>Inovaud vous offre un ensemble de services
            basés sur les technologies de pointe.
          </Text>
        </Grid>
        <br></br>
        <br></br>
        <Card className="services-tab">
          <Grid.Container>
            {servicesArray.map(({ label, icon, link }) => (
              <Grid xs={12} md={3} sm={6} key={label}>
                <Link href={link}>
                  <Grid className="service-tab hover-tab">
                    <Grid className="hover-zoom">
                      <Image
                        objectFit="contain"
                        src={icon}
                        alt=""
                        width={60}
                        height={60}
                      />
                    </Grid>
                    <Grid
                      css={{
                        textAlign: "center",
                        width: "50%",
                        margin: "10px auto",
                      }}
                    >
                      <Text span>{label}</Text>
                    </Grid>
                  </Grid>
                </Link>
              </Grid>
            ))}
          </Grid.Container>
        </Card>
      </Grid>
    </>
  );
};
