import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import { Contact } from "../contact";

export const ProcessMotion = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Text span className={"number"}>
                1
              </Text>
              <Grid className="block text-left">
                <Text span className="title">
                  Étude de vos besoins
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos besoins afin de vous offrir une solution
                  pertinente et sur mesure.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Conceptualisation
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous imaginons et vous proposons une identité visuelle
                  générale.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper3"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Design
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous transmettons vos idées et votre vision au travers
                  d'animations attrayantes et pertinentes.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Animation
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous donnons vie à la conception par le mouvement et la
                  synchronisation.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Conception sonore
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous ajoutons des éléments audio pour améliorer l'animation et
                  optimiser la transmission de votre message.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Itération et rétroaction
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous accueillons votre feedback et procédons à des
                  ajustements.
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
