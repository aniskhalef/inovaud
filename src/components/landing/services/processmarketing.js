import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import { Contact } from "../contact";

export const ProcessMarketing = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Text span className={"number"}>
                1
              </Text>
              <Grid className="block text-left">
                <Text span className="title">
                  Étude de vos besoins
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos besoins afin de vous offrir une solution
                  pertinente et sur mesure.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Étude de marché
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous menons une étude de marché globale en identifiant vos
                  concurrents, les tendances actuelles, vos opportunités et
                  défis.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper3"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Définition du but et des objectifs
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous définissons les objectifs de votre entreprise pour votre
                  campagne de promotion, identifions et mesurons vos indicateurs
                  clés de performance (KPI).
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Developpement de stratégie
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous élaborons en étroite collaboration avec vous une
                  stratégie de marketing digitale adaptée à vos objectifs, en
                  recourant à différents canaux et techniques.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Gestion des réseaux sociaux{" "}
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous accordons aux réseaux sociaux l'importance qui leur est
                  due pour la promotion de votre entreprise.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  SEO
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous vous aidons à paramétrer votre référencement naturel (SEO
                  – Search Engine Optimization) afin que votre site web soit
                  bien positionné dans le SERP (pages de résultats des moteurs
                  de recherche) et donc plus consulté que ceux de vos
                  concurrents.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper7"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  7
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Analyse et repoting
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous préparons des rapports réguliers de votre campagne de
                  marketing digitale et continuons à affiner votre stratégie en
                  analysant les données recueillies pour obtenir de meilleurs
                  résultats.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper8"}>
            <Grid className={"top"}>
              <Grid className=" text-left">
                <Text span className="title">
                  Langages utilisés
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  JAVASCRIPT / PYTHON / C++ / C# / JAVA / PHP / RUBY / SWIFT /
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
