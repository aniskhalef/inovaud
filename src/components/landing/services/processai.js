import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import { Contact } from "../contact";

export const ProcessAi = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  1
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Définition de la problématique
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos enjeux et les bénéfices d'un recours à l'IA
                  ou à l'apprentissage automatique.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Collecte Et Préparation Des Données
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous collectons et trions des données provenant de différentes
                  sources.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper3">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Développement et formation du modèle
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous développons et entraînons le modèle d'IA au moyen d'un
                  algorithme pertinent.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Évaluation et validation du modèle
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous évaluons la performance du modèle pour garantir sa
                  fiabilité et sa précision.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Déploiement
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous déployons le modèle d'IA dans un environnement de
                  production.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Surveillance et maintenance
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous surveillons la performance du modèle et nous engageons à
                  l'ajuster et à le perfectionner continuellement.
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
