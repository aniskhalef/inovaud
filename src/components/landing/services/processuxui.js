import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import { Contact } from "../contact";

export const ProcessUxUi = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Text span className={"number"}>
                1
              </Text>
              <Grid className="block text-left">
                <Text span className="title">
                  Étude de vos besoins
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos besoins afin de vous offrir une solution
                  pertinente et sur mesure.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Benchmarking
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous analysons et référençons votre marché pour vous créer un
                  avantage concurrentiel.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper3"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Création du userflow (UX)
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous créons un visuel qui décrit comment un utilisateur
                  interagit avec un site web ou une application pour atteindre
                  un objectif spécifique.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Création de wireframes
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous réalisons une représentation visuelle basse-fidélité de
                  l'interface utilisateur.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Création des interfaces (UI)
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous utilisons des outils de conception pour créer une
                  interface utilisateur attrayante et fonctionnelle.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Prototypage
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous créons des prototypes interactifs d'interfaces
                  utilisateur.
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
