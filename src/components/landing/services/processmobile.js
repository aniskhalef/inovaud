import { Grid, Card, Text } from "@nextui-org/react";
import { Contact } from "../contact";

export const ProcessMobile = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Text span className={"number"}>
                1
              </Text>
              <Grid className="block text-left">
                <Text span className="title">
                  Étude de vos besoins
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos besoins afin de vous offrir une solution qui
                  tienne compte des attentes de vos utilisateurs.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Conception de l&apos;interface utilisateur
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous concevons les interfaces de votre application mobile en
                  accordant un soin particulier à l'expérience utilisateur.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper3"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Développement de l&apos;application
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous adoptons les langages de programmation pertinents pour
                  développer une application qui réponde à des très hautes
                  normes de qualité et de sécurité.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Test de l&apos;application
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous testons votre application pour nous assurer qu'elle est
                  conforme à vos attentes et donc prête à être mise en service.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Publication de l&apos;application
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous publions votre application sur les plate-formes les plus
                  pertinentes, comme l'App Store pour iOS ou Google Play Store
                  pour Android.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Maintenance et mise à jour
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous veillons au bon fonctionnement de votre application et
                  procédons à d'éventuelles adaptations.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper8"}>
            <Grid className={"top"}>
              <Grid className=" text-left">
                <Text span className="title">
                  Langages utilisés
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  JAVASCRIPT / PYTHON / C++ / C# / JAVA / PHP / RUBY / SWIFT /
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
