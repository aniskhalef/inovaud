import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import { Contact } from "../contact";

export const ProcessWeb = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Text span className={"number"}>
                1
              </Text>
              <Grid className="block text-left">
                <Text span className="title">
                  Identification de vos besoins
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos besoins afin de vous offrir une solution
                  pertinente et sur mesure.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Conception
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous concevons l'aspect visuel du site en créant des maquettes
                  et des wireframes pour la mise en page du contenu.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper3"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Développement front-end
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous exploitons des langages de programmation pour la
                  construction de la partie visible du site.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Développement back-end
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous adoptons des langages de programmation pour la
                  construction des fonctionnalités du site.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Tests et lancement
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous testons votre site pour nous assurer qu'il est conforme à
                  vos attentes et prêt à être mis en service.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Maintenance et mise à jour
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous veillons au bon fonctionnement de votre site et procédons
                  à d'éventuelles adaptations.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper7"}>
            <Grid className={"top"}>
              <Grid className=" text-left">
                <Text span className="title">
                  Langages utilisés
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  JAVASCRIPT / PYTHON / C++ / C# / JAVA / PHP / RUBY / SWIFT /
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
