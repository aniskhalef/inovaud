import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import { Contact } from "../contact";

export const ProcessBlockchain = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Text span className={"number"}>
                1
              </Text>
              <Grid className="block text-left">
                <Text span className="title">
                  Étude de vos besoins
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos besoins afin de vous offrir une solution
                  pertinente et sur mesure en blockchain.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Conception De L'architecture
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Pour garantir la bonne architecture de votre solution, nous
                  choisissons avec soin la plate-forme blockchain appropriée,
                  les protocoles de consensus, les langages de programmation
                  ainsi que les outils nécessaires.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper3"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Développement du smart contract
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous écrivons le code de smart contract en utilisant les
                  langages de programmation appropriés.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Développement de l'application client
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous développons l'application client qui interagit avec le
                  smart contract.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Test et déploiement
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous réalisons des tests pour assurer le bon fonctionnement et
                  la conformité de la solution blockchain. Celle-ci peut ensuite
                  être déployée sur le réseau de blockchain approprié.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Maintenance et mise à jour
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous assurons la maintenance de votre solution blockchain et
                  procédons aux adaptations nécessaires.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper7"}>
            <Grid className={"top"}>
              <Grid className=" text-left">
                <Text span className="title">
                  Langages utilisés
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  JAVASCRIPT / PYTHON / C++ / C# / JAVA / PHP / RUBY / SWIFT /
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
