import { Grid, Card, Text } from "@nextui-org/react";
import Image from "next/image";
import { Contact } from "../contact";

export const ProcessIot = () => {
  return (
    <Grid align="center" css={{ marginTop: "10%" }}>
      <Grid>
        <Text h1>Les étapes clés de votre solution</Text>
        <br></br>
        <br></br>
        <Grid md={8} className="service-container">
          <Grid className="wrapper1">
            <Grid className="top">
              <Text span className={"number"}>
                1
              </Text>
              <Grid className="block text-left">
                <Text span className="title">
                  Étude de vos besoins
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous étudions vos besoins afin de vous offrir une solution
                  pertinente en IoT.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text span className={"number"}>
                  2
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Conception Du Système
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous fournissons une architecture de système IoT qui analyse
                  les capteurs, les dispositifs et les plate-formes nécessaires
                  au projet.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper3"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  3
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Sélection des capteurs et des dispositifs
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous choisissons les capteurs et les dispositifs qui répondent
                  aux besoins spécifiques de l'application.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper4"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  4
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Développement des logiciels
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous développons les logiciels nécessaires à l'acquisition, la
                  communication et l'analyse des données.
                </Text>
              </Grid>
            </Grid>
          </Grid>{" "}
          {/**/}
          <Grid className={"wrapper5"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  5
                </Text>
              </Grid>

              <Grid className="block text-left">
                <Text span className="title">
                  Intégration des données
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  Nous utilisons des protocoles de communication standards afin
                  d'intégrer les données collectées à partir des différents
                  capteurs et dispositifs.
                </Text>
              </Grid>
            </Grid>
          </Grid>
          {/**/}
          <Grid className={"wrapper6"}>
            <Grid className={"top"}>
              <Grid>
                <Text span className={"number"}>
                  6
                </Text>
              </Grid>
              <Grid className="block text-left">
                <Text span className="title">
                  Déploiement, surveillance et maintenance
                </Text>
                <br></br>
                <br></br>
                <Text span className="description">
                  La performance de votre système est assurée par une
                  surveillance et une maintenance continue.
                </Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Contact />
      </Grid>
    </Grid>
  );
};
