import { Text, Grid, Button } from "@nextui-org/react";
import React from "react";
import Link from "next/link";
import Lottie from "react-lottie";
import Animation from "/public/assets/lotties/hero.json";

export const Hero = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={300}
        width={350}
        style={{ pointerEvents: "none" }}
      />
    );
  }

  return (
    <>
      <Grid.Container
        gap={2}
        align={"center"}
        css={{
          alignItems: "center",
          justifyContent: "center",
          margin: "auto",
          marginTop: "4em",
          "@smMin": {
            padding: "8em",
          },
        }}
      >
        <Grid
          md={6}
          sm={12}
          xs={12}
          css={{
            textAlign: "left",
            width: "100%",
          }}
        >
          <Grid>
            <Text h1 style={{ letterSpacing: "2px" }}>
              Votre vision, nos solutions
            </Text>
            <br></br>
            <Text className="content" span>
              Sur mesure et innovante, celle-ci sera le fruit de notre étroite
              et créative collaboration<br></br> Vous avez un projet, une
              vision. <br></br>Son déploiement requiert un outil, une solution.
              Sur mesure et innovante.
              <br></br> Celle-ci sera le fruit de notre étroite et créative
              collaboration.
            </Text>

            <br></br>
            <br></br>
            <Grid>
              <Link href="/services">
                <button className="main-button">Découvrez nos services</button>
              </Link>
            </Grid>
            <br></br>
            <br></br>
          </Grid>
        </Grid>

        <Grid md={6} sm={12} xs={12} style={{ alignItems: "center" }}>
          <HandleAnimation />
        </Grid>
      </Grid.Container>
    </>
  );
};
