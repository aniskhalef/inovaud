import { Grid, Text, Card } from "@nextui-org/react";
import React from "react";
const Fade = require("react-reveal/Fade");
import Image from "next/image";

export const Workflow = () => {
  const workflow = [
    {
      icon: "/assets/landing/workflow_contact.svg",
      label:
        "Contact avec l'un de nos experts et identification de vos besoins",
      text: "L'équipe Inovaud adopte une approche collaborative pour identifier vos besoins et vos objectifs personnels.",
    },
    {
      icon: "/assets/landing/workflow_signature.svg",
      label: "Signature d'un accord de confidentialité",
      text: "Notre engagement à protéger vos données sensibles et vos secrets commerciaux est absolu.",
    },
    {
      icon: "/assets/landing/workflow_etude.svg",
      label: "Étude de faisabilité",
      text: "Nous vous assistons dans la prise de décisions pertinentes quant à l’opportunité d’aller de l’avant avec votre projet, de l'ajuster ou de le réorienter.",
    },
    {
      icon: "/assets/landing/workflow_prop.svg",
      label: "Proposition de solutions optimisées en terme de coûts et délais",
      text: "Nous élaborons un plan détaillé qui décrit chaque phase du projet, y compris les jalons et les produits livrables.",
    },
    {
      icon: "/assets/landing/workflow_plan.svg",
      label: "Planification et fixation des milestones",
      text: "Nous élaborons un plan détaillé qui décrit chaque phase du projet, y compris les jalons et les produits livrables.",
    },

    {
      icon: "/assets/landing/workflow_impl.svg",
      label: "Implémentation des solutions proposées",
      text: "Nous vous assurons une implémentation efficace et un traitement de tous les défis en temps opportun.",
    },
    {
      icon: "/assets/landing/workflow_test.svg",
      label: "Test, validation et recueil de feedback",
      text: "Nous évaluons votre solution afin de garantir sa haute qualité et ainsi la satisfaction de vos clients.",
    },
    {
      icon: "/assets/landing/workflow_opt.svg",
      label: "Maintenance, adaptation et amélioration",
      text: "Nous améliorons continuellement votre solution en l'adaptant à un contexte en évolution.",
    },
  ];
  return (
    <>
      <Grid md={12} style={{ padding: "4em" }}>
        <Grid css={{ textAlign: "center" }}>
          <Text h1>NOTRE WORKFLOW</Text>
        </Grid>
        <br></br>
        <br></br>
        <Grid.Container gap={2}>
          {workflow.map(({ label, icon, text }) => (
            <Grid xs={12} md={3} sm={4} key={label}>
              <Card className="workflow-tab ">
                <Card.Body css={{ alignItems: "center" }}>
                  <Grid.Container
                    gap={1}
                    css={{
                      alignItems: "center",
                      textAlign: "left",
                      height: "100%",
                    }}
                  >
                    <Grid md={2} sm={2}>
                      <Image
                        src={icon}
                        alt=""
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                    </Grid>
                    <Grid
                      md={10}
                      sm={10}
                      css={{
                        height: "100%",
                      }}
                    >
                      <Text span css={{ fontWeight: "bold", fontSize: "16px" }}>
                        {label}
                      </Text>
                    </Grid>
                  </Grid.Container>
                  <br></br>
                  <Text span css={{ fontSize: "15px" }}>
                    {text}
                  </Text>
                </Card.Body>
              </Card>
            </Grid>
          ))}
        </Grid.Container>
      </Grid>
    </>
  );
};
