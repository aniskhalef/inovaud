import { Grid, Text } from "@nextui-org/react";
import React from "react";
import Lottie from "react-lottie";
import Animation from "/public/assets/lotties/robot.json";
import Link from "next/link";

export const Contact = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={250}
        width={250}
        style={{
          pointerEvents: "none",
        }}
      />
    );
  }
  return (
    <>
      <Grid align="center" css={{ marginTop: "10em" }}>
        <Grid.Container
          gap={2}
          className="contact"
          css={{
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Grid
            md={6}
            sm={12}
            xs={12}
            css={{
              alignItems: "center",
              justifyContent: "center",
              textAlign: "left",
            }}
          >
            <Grid>
              {" "}
              <Text h1 size={"$2xl"}>
                FIXER UN PREMIER RENDEZ-VOUS
              </Text>
              <br></br>
              <Text span size={"$lg"}>
                Vous avez des questions, des attentes, des besoins.
              </Text>
              <Text span size={"$lg"}>
                Une première rencontre vous orientera vers des solutions.
              </Text>
            </Grid>

            <Grid>
              <Link href="/rendez-vous">
                <button className="main-button">Contactez-nous</button>
              </Link>
            </Grid>
          </Grid>
          <Grid md={6} sm={12} xs={12} style={{ alignItems: "center" }}>
            <HandleAnimation />
          </Grid>
        </Grid.Container>
      </Grid>
    </>
  );
};
