import { Grid, Input, Text } from "@nextui-org/react";
import React, { useState } from "react";
const Bounce = require("react-reveal/Bounce");
const Fade = require("react-reveal/Fade");
import Lottie from "react-lottie";
import Animation from "/public/assets/lotties/diamond.json";

export const Value = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        style={{ width: "100%", pointerEvents: "none", paddingTop: "5em" }}
        height={400}
        width={400}
      />
    );
  }

  return (
    <>
      <Grid.Container
        align={"center"}
        style={{
          padding: "5em",
          display: "inline-block",
          alignItems: "center",
          justifyContent: "center",
          margin: "auto",
        }}
      >
        <Grid md={6} sm={12} xs={12}>
          <HandleAnimation />
        </Grid>
        <Grid
          md={6}
          sm={12}
          xs={12}
          css={{
            textAlign: "left",
          }}
        >
          <Text h1>NOS VALEURS FONDAMENTALES</Text>
          <br></br>
          <Grid css={{ fontSize: "25px", width: "90%" }}>
            <Grid>
              <Text b style={{ color: "#3C8A2E" }}>
                Innovation:{" "}
              </Text>
              <Text span>
                nous recourrons aux savoirs technologiques les plus pointus, les
                plus récents et les plus pertinents
              </Text>
            </Grid>{" "}
            <br></br>
            <Grid>
              <Text b style={{ color: "#3C8A2E" }}>
                Collaboration:{" "}
              </Text>
              <Text span>
                nous cherchons à comprendre vos besoins et à élaborer avec vous
                les meilleures solutions
              </Text>
            </Grid>{" "}
            <br></br>
            <Grid>
              <Text b style={{ color: "#3C8A2E" }}>
                Perfectionnement:{" "}
              </Text>
              <Text span>
                nous cherchons systématiquement à améliorer l'existant et à
                obtenir de meilleurs résultats
              </Text>
            </Grid>
          </Grid>
        </Grid>
      </Grid.Container>
    </>
  );
};
