import { Grid, Input, Text } from "@nextui-org/react";
import React from "react";
import Lottie from "react-lottie";
import Animation from "/public/assets/lotties/teamwork.json";

export const Solutions = () => {
  function HandleAnimation() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: Animation,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <Lottie
        options={defaultOptions}
        height={350}
        width={350}
        style={{ pointerEvents: "none", paddingTop: "5em" }}
      />
    );
  }

  return (
    <>
      <Grid.Container
        gap={2}
        align={"center"}
        css={{
          "@smMin": {
            padding: "8em",
          },
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Grid md={6} sm={12} xs={12}>
          <HandleAnimation />
        </Grid>
        <Grid
          md={6}
          sm={12}
          xs={12}
          css={{
            alignItems: "center",
            textAlign: "left",
          }}
        >
          <Grid>
            <Text h1 style={{ letterSpacing: "2px" }}>
              Vous doter d'une solution sur mesure et innovante
            </Text>
            <br></br>

            <Text size={"$lg"} span>
              Nos solutions sont inédites pour chacun de nos clients : elles
              sont le fruit d'une collaboration basée sur la consultation,
              l'identification de vos besoins, l'élaboration, le suivi et
              l'amélioration continue. Notre objectif ? Vous doter d'un outil
              qui serve votre vision. Celui-ci fera appel aux savoirs
              technologiques les plus pointus, les plus récents, les plus
              pertinents. Le mot « innovation » n'a de sens pour nous que s'il
              signifie « amélioration ». Nous sommes portés vers le
              perfectionnement. Notre travail est notre passion.
            </Text>
          </Grid>
        </Grid>
      </Grid.Container>
    </>
  );
};
