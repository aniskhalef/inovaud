import { Grid, Text, Card } from "@nextui-org/react";
import React from "react";
const Fade = require("react-reveal/Fade");
import Image from "next/image";
import Link from "next/link";

export const Offers = () => {
  const workflow = [
    {
      title: "Des developpeurs web",
      date: "03/Avril 2023",
      text: "Pour postuler au poste de Développeur Web, soumettez une copie de votre CV et lettre de motivation.",
      link: "/carriere/developpeur-web",
    },
    {
      title: "Des developpeurs web",
      date: "03/Avril 2023",
      text: "Pour postuler au poste de Développeur Web, soumettez une copie de votre CV et lettre de motivation.",
      link: "/carriere/developpeur-web",
    },
    {
      title: "Des developpeurs web",
      date: "03/Avril 2023",
      text: "Pour postuler au poste de Développeur Web, soumettez une copie de votre CV et lettre de motivation.",
      link: "/carriere/developpeur-web",
    },
    {
      title: "Des developpeurs web",
      date: "03/Avril 2023",
      text: "Pour postuler au poste de Développeur Web, soumettez une copie de votre CV et lettre de motivation.",
      link: "/carriere/developpeur-web",
    },
  ];
  return (
    <>
      <Grid
        css={{
          padding: "6em",
        }}
      >
        <Grid>
          <Text h1>Nos Offres</Text>
        </Grid>
        <br></br>
        <Grid.Container
          gap={2}
          css={{
            "@xsMax": {
              padding: 0,
            },
          }}
        >
          {workflow.map(({ title, date, text, link }) => (
            <Grid xs={12} md={3} sm={6} key={title}>
              <Card className="career-tab career-hover-tab ">
                <Link href={link}>
                  <Card.Body>
                    <Grid.Container css={{ display: "block", padding: "2px" }}>
                      <Grid>
                        <Text
                          span
                          css={{
                            fontWeight: "bold",
                            fontSize: "18px",
                          }}
                        >
                          {title}
                        </Text>
                      </Grid>
                      <hr
                        style={{
                          border: "solid #fff 1px",
                          marginBottom: "8px",
                        }}
                      ></hr>

                      <Grid css={{ marginBottom: "16px" }}>
                        <Text span size={"$md"}>
                          {date}
                        </Text>
                      </Grid>
                      <Grid>
                        <Text span size={"$md"}>
                          {text}
                        </Text>
                      </Grid>
                    </Grid.Container>
                  </Card.Body>
                </Link>
              </Card>
            </Grid>
          ))}
        </Grid.Container>
      </Grid>
    </>
  );
};
