/** @type {import('next').NextConfig} */


const withPWA = require("next-pwa")({
  dest: "public",
  register: true,
});

const nextConfig = withPWA({
  reactStrictMode: true,
  swcMinify: true,
    images: {
      domains: ['i.gifer.com'],
      formats: ['image/avif'],
    }
});
module.exports = nextConfig;



